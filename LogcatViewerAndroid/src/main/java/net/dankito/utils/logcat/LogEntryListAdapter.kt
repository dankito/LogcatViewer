package net.dankito.utils.logcat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.list_item_log_entry.view.*
import java.text.SimpleDateFormat


open class LogEntryListAdapter(protected val logEntries: List<LogEntry> = listOf()) : BaseAdapter() {

    companion object {
        private val DateTimeFormat = SimpleDateFormat("dd.MM HH:mm:ss.SSS")
    }


    override fun getCount(): Int {
        return logEntries.size
    }

    override fun getItem(index: Int): LogEntry {
        return logEntries[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getView(index: Int, convertView: View?, parent: ViewGroup): View {
        val view = if (convertView != null) convertView
        else {
            val inflator = LayoutInflater.from(parent.context)
            inflator.inflate(R.layout.list_item_log_entry, parent, false)
        }

        val logEntry = getItem(index)

        view.txtvwMessage.text = logEntry.message
        view.txtvwLogLevel.text = logEntry.level.name
        view.txtvwLogTag.text = logEntry.logTag

        view.txtvwDateTime.text = DateTimeFormat.format(logEntry.dateTime)

        return view
    }


    fun logEntriesUpdatedOnUiThread() {
        notifyDataSetChanged()
    }

}