package net.dankito.utils.logcat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.slf4j.LoggerFactory


class MainActivity : AppCompatActivity() {

    companion object {
        private val log = LoggerFactory.getLogger(MainActivity::class.java)
    }


    private val logCollector = LogcatLogCollector()

    private val logEntries = mutableListOf<LogEntry>()

    private val logEntryListAdapter = LogEntryListAdapter(logEntries)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        lstvwLogEntries.adapter = logEntryListAdapter

        logCollector.startAsync { logEntry -> showLogEntry(logEntry) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
//            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun showLogEntry(logEntry: LogEntry) {
        runOnUiThread {
            showLogEntryOnUiThread(logEntry)
        }
    }

    private fun showLogEntryOnUiThread(logEntry: LogEntry) {
        logEntries.add(logEntry)

        logEntryListAdapter.logEntriesUpdatedOnUiThread()
    }

}
