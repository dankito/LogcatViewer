package net.dankito.utils.logcat


enum class LogLevel(val logLevelChar: String) {

    Verbose("V"),
    Debug("D"),
    Info("I"),
    Warning("W"),
    Error("E");


    companion object {

        fun parse(logLevelChar: String): LogLevel? {
            return LogLevel.values().firstOrNull { it.logLevelChar == logLevelChar }
        }

    }

}