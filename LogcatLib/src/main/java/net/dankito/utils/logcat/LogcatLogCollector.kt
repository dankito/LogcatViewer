package net.dankito.utils.logcat

import org.slf4j.LoggerFactory
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


open class LogcatLogCollector {

    companion object {
        private val DateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

        private val log = LoggerFactory.getLogger(LogcatLogCollector::class.java)
    }


    var isRunning = false
        protected set


    open fun startAsync(listener: (LogEntry) -> Unit) {
        thread {
            start(listener)
        }
    }

    open fun start(listener: (LogEntry) -> Unit) {
        if (isRunning) {
            return
        }

        try {
            isRunning = true
            val process = Runtime.getRuntime().exec("logcat")

            process.inputStream.reader().buffered().use { bufferedReader ->
                var line = bufferedReader.readLine()

                while (isRunning && line != null) {
                    handleLogLine(line, listener)

                    line = bufferedReader.readLine()
                }
            }

            log.info("Reading logs from Logcat stopped")
        } catch (e: Exception) {
            log.error("Could not read logs from Logcat", e)
            isRunning = false
        }

    }

    open fun stop() {
        isRunning = false
    }


    protected open fun handleLogLine(line: String, listener: (LogEntry) -> Unit) {
        try {
            parseLogLine(line)?.let { logEntry ->
                listener(logEntry)
            }
        } catch (e: Exception) {
            log.error("Could not handle log line '$line'", e)
        }
    }

    protected open fun parseLogLine(line: String): LogEntry? {
        val parts = line.trim().split(" ").filter { it.isNotBlank() }

        if (parts.size < 7) {
            return null
        }

        val date = parts[0]
        val time = parts[1]
        val pidString = parts[2]
        val tidString = parts[3]
        val logLevelString = parts[4]

        var logTag = parts[5].trim()
        if (logTag.endsWith(':')) {
            logTag = logTag.substring(0, logTag.length - 1).trimEnd()
        }

        var message = parts.subList(6, parts.size).joinToString(" ").trim()
        if (message.startsWith(':')) {
            message = message.substring(1).trimStart()
        }

        return LogEntry(
            message, parseLogLevel(logLevelString), logTag, parseDateTime(date, time),
            pidString.toIntOrNull() ?: -1, tidString.toIntOrNull() ?: -1
        )
    }

    protected open fun parseLogLevel(logLevelString: String): LogLevel {
        return LogLevel.parse(logLevelString)
            ?: LogLevel.Verbose // TODO: what to do in latter case
    }

    /**
     * monthAndDay has format <month>-<day>.
     * time has format <hours>:<minutes>:<seconds>.<milliseconds>
     */
    protected open fun parseDateTime(monthAndDay: String, time: String): Date {
        val year = getYearForMonthAndDay(monthAndDay)
        val dateTimeString = "$year-$monthAndDay $time"

        return DateTimeFormat.parse(dateTimeString)
    }

    protected open fun getYearForMonthAndDay(monthAndDay: String): String {
        return "2019" // TODO
    }
}