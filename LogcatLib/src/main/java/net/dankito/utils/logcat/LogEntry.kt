package net.dankito.utils.logcat

import java.util.*


open class LogEntry(val message: String,
                    val level: LogLevel,
                    val logTag: String,
                    val dateTime: Date,
                    val pid: Int = -1,
                    val tid: Int = -1
)